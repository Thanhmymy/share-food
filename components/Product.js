import Image from "next/image"
import Link from "next/link"



function Product({ id, name, imageUri, description, quantity, chefId, price }) {
    const url = "http://localhost:3000/products/" + id

    return (
        <div className="p-5 ">
            <div className="flex flex-col text-center items-center lg:m-0 hover:shadow-xl   mr-auto ml-auto mt-8 hover:border hover:border-[#62BF5B] lg:w-4/4 lg:h-full">
                <p  className="lg:mb-4  w-full p-2 text-xl text-shadow border-b">{name}</p>
                <Image alt="photo" src={imageUri} height={150} width={150} objectFit="cover" />
                <p className="lg:mt-2">{price}€</p>
                <p>Quantité : {quantity}</p>
                <p className="lg:line-clamp-1 p-1">Description : {description}</p>
                <div>


                    
                </div>
                <Link passHref={true} href={url}>
                        <button className="border mt-4 p-2 w-full text-white bg-[#62BF5B]">Voir le plat </button>
                        </Link>
            </div>

        </div>

    )
}

export default Product
