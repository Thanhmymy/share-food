import { useState } from "react"
import { CheckIcon, MenuIcon, XIcon } from "@heroicons/react/solid"

function Collapse() {
    const [isOpen, setIsOpen] = useState(false)
    const [open, setOpen] = useState(false)
    const [ouvrir, setOuvrir] = useState(false)
    return (
        <>
            <div className="border flex items-center justify-between p-5 border-gray-800 mt-8">
                <div>
                    <div className="collapsible">
                        <button onClick={() => setIsOpen(!isOpen)} className="border-b border-black hover:text-[#62BF5B]">Commande de 124  </button>
                        {isOpen && <div className="content">
                            <p className=" ml-3 ">Plat : Tiramisu</p>
                            <p className="ml-3">Quantité: 1</p>
                            <p className="ml-3">Prix : 3€</p>
                            <p className="ml-3">Adresse : 34 Rue Antoine Primat</p>
                        </div>}
                    </div>

                </div>
                <div className="flex">
                    <CheckIcon className="h-10 border rounded-md text-green-600 border-black" />
                    <XIcon className="h-10 border text-red-700 rounded-md ml-2 border-black" />
                </div>

            </div>

            <div className="border flex items-center justify-between p-5 border-gray-800 mt-8">
                <div>
                    <div className="collapsible">
                        <button onClick={() => setOpen(!open)} className="border-b border-black hover:text-[#62BF5B] ">Commande de 125  </button>
                        {open && <div className="content">
                            <div>
                                    <p className=" ml-3 ">Plat : Rougail Saucisse</p>
                                    <p className="ml-3">Quantité: 1</p>
                                    <p className="ml-3">Prix : 10€</p>
                                    <p className="ml-3">Adresse : 34 Rue Antoine Primat</p>
                            </div>

                        </div>}
                    </div>

                </div>
                <div className="flex">
                    <CheckIcon className="h-10 border rounded-md text-green-600 border-black" />
                    <XIcon className="h-10 border text-red-700 rounded-md ml-2 border-black" />
                </div>

            </div>

            <div className="border flex items-center justify-between p-5 border-gray-800 mt-8">
                <div>
                    <div className="collapsible">
                        <button onClick={() => setOuvrir(!ouvrir)} className="border-b border-black hover:text-[#62BF5B]">Commande de 126  </button>
                        {ouvrir && <div className="content">
                            <p className=" ml-3 ">Plat : Soupe de potirons</p>
                            <p className="ml-3">Quantité: 2</p>
                            <p className="ml-3">Prix : 10€</p>
                            <p className="ml-3">Adresse : 34 Rue Antoine Primat</p>
                        </div>}
                    </div>

                </div>
                <div className="flex">
                    <CheckIcon className="h-10 border rounded-md text-green-600 border-black" />
                    <XIcon className="h-10 border text-red-700 rounded-md ml-2 border-black" />
                </div>

            </div>
</>
    )
}

export default Collapse
