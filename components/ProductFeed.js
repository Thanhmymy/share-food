import Product from "./Product"

function ProductFeed({products}) {
    return (
        <div className="lg:grid lg:grid-cols-4 lg:justify-center lg:mt-5 ">
            {products.slice(0).map(({ id, name, imageUri, description, quantity, chefId, price }) => (
                <Product
                    key={id}
                    id={id}
                    name={name}
                    imageUri={imageUri}
                    description={description}
                    quantity={quantity}
                    chefId={chefId}
                    price={price}
                />
            ))}
        </div>
    )
}

export default ProductFeed