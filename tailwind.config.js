module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}','./public/**/*.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: theme => ({
        'banniere': "url('/Assets/banniere.png')",
        'banniereo': "url('/Assets/banniereo.png')",
      }),
      transitionDuration: {
        '0': '0ms',
        '2000': '2000ms',
      }
    },
  },
  variants: {
    extend: {
      transitionDuration: ['hover', 'focus'],
    },
  },
  plugins: [require("@tailwindcss/line-clamp")],
}
