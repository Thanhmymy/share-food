import { MenuIcon, SearchIcon } from '@heroicons/react/solid'
import React, { useState } from 'react'
import { Modal } from 'react-bootstrap';
import ProductFeed from '../../components/ProductFeed';
import Image from "next/image"
import Link from "next/link"

function Restaurant({ productsProp }) {
    const [show, setShow] = useState(false);
    const [products, setProducts] = useState(productsProp)
    const [name, setName] = useState('')
    const [price, setPrice] = useState('')
    const [imageUri, setImage] = useState('')
    const [quantity, setQuantity] = useState('')
    const [description, setDescription] = useState('')

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleSubmit = (e) => {
        e.preventDefault(); 
        const form = { name, price, imageUri, quantity, description };

        
        fetch('http://localhost:8000/plat',{
            method: 'POST',
            headers: {"Content-Type" : "application/json"},
            body: JSON.stringify(form)
        }).then((response)=> response.json())
        .then((response) =>{
            console.log(response)
            setProducts([...products, response])
        })

    }


    return <div className="lg:w-4/5 lg:mr-auto lg:ml-auto">
        <header className="flex justify-between">
            <div className="flex items-center">
                <MenuIcon className="h-14" />
                <Link passHref={true} href="/">
                    <Image alt="logo" width={150} height={50} src="/Assets/VERT_HD.png" className="mr-auto ml-auto h-5  lg:h-10" />
                </Link>
                <p className="text-lg font-bold">Pro</p>
            </div>
            <div className="">
                <button className="border border-[#55a74f] rounded-md p-2 lg:p-3 bg-[#62BF5B] text-white mt-2" onClick={handleShow}>Add product</button>


                <Modal show={show} onHide={handleClose} className="absolute inset-x-0 top-20  bg-white flex mr-auto ml-auto border border-[#62BF5B] p-5 lg:w-3/6 shadow-xl">
                    <Link passHref="true" href="" className="text-xl flex justify-end cursor-pointer" onClick={handleClose}>x</Link>
                    <h2 className="text-center mb-8 text-xl">Add a product</h2>
                    
                    <form className=" flex flex-col" onSubmit={handleSubmit}>
                        <div className="text-center">
                            <p >Nom du plat :</p>
                            <input 
                            required
                            value={name}
                            onChange={(e)=> setName(e.target.value)}
                            placeHolder="Burger,salade..."
                            className="border w-full" 
                            type="text" />
                        </div>

                        <div className="text-center mt-5">
                            <p >Image: </p>
                            <input 
                            required
                            value={imageUri}
                            onChange={(e)=> setImage(e.target.value)}
                            placeHolder="https://...."
                            className="border  w-full" type="text" />
                        </div>

                        <div className="text-center mt-5">
                            <p >Prix : </p>
                            <input 
                            required 
                            value={price}
                            onChange={(e)=> setPrice(e.target.value)}
                            className="border  w-full" type="number" min="1" max="100" />
                        </div>

                        <div className="text-center mt-5">
                            <p >Quantité </p>
                            <input 
                            required 
                            value={quantity}
                            onChange={(e)=> setQuantity(e.target.value)}
                            className="border  w-full" type="number" min="1" max="100" />
                        </div>

                        <div className="text-center mt-5">
                            <p >Description: </p>
                            <textarea 
                            required 
                            value={description}
                            onChange={(e)=> setDescription(e.target.value)}
                            placeholder="Ingrédient..."
                            className="border  w-full" type="text" />
                        </div>



                    
                    <Modal.Footer className="flex justify-around mt-8">
                        <button className="border p-2 bg-[#62BF5B] text-white">
                            Save Changes
                        </button>
                    </Modal.Footer>
                    </form>
                </Modal>


            </div>
        </header>

    <div className=" lg:bg-cover bg-contain bg-no-repeat bg-center mt-1 border-b">
        <Image alt="banniere" width={1350} height={500} objectFit="cover" src="/Assets/banniereo.png" className="lg:w-full lg:h-96 mt-1"/>
    </div>

        <div>
            <h2 className="text-center text-2xl mt-5">Produits</h2>
            <ProductFeed products={products} />
        </div>


    </div>


}


export async function getServerSideProps(context) {
    const productsProp = await fetch("http://localhost:8000/plat").then(
        (res) => res.json()
    );

    return {
        props: {
            productsProp,
        }
    }
}



export default Restaurant;
