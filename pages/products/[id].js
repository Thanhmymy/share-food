import axios from 'axios'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { MenuIcon, PencilIcon, XIcon } from '@heroicons/react/solid';
import Image from 'next/image'
import Link from 'next/link'

function ProductPage() {
    const router = useRouter()
    const {id} = router.query;

    const url = `http://localhost:8000/plat/${id}`
    const [product,setProduct] = useState(null)
    let content=null

    useEffect(() => {

        if(id){
            axios.get(url).then(response => {
                setProduct(response.data)
            })
        }


},[id,url])
console.log(url);
if(product){
    content =
    <div className="lg:w-4/5 lg:mr-auto lg:ml-auto">

        <header className="flex justify-between">
        <div className="flex items-center">
                <MenuIcon className="h-14" />
                <Link passHref={true} href="/products">
                    <Image width={150} height={50} src="/Assets/VERT_HD.png" alt="logo" className="mr-auto ml-auto h-5  lg:h-10" />
                </Link>
                <p className="text-lg font-bold">Pro</p>
            </div>
        </header>


        <div className=" border border-black shadow-2xl lg:flex w-3/4 mr-auto ml-auto mt-20 mb-14  ">
            <div className="p-5">
                <Image alt="photo" width={500} height={425} objectFit='cover' src={product.imageUri}/>
            </div>

            <div className="bg-white  lg:p-5 p-2 lg:w-3/4 h-full">
                <div className="flex justify-between items-center">
                
                <p className="text-3xl font-bold text-black  mt-8">{product.name}</p>
                <div className="flex">
                    <PencilIcon className="h-10 hover:text-[#62BF5B] text-black border border-black rounded-md mr-2"/>
                    <XIcon className="h-10 text-black hover:text-red-500 border border-black rounded-md"/>
                </div>

                </div>
                
                <p className="text-3xl font-bold text-black  mt-8">{product.price} €</p>
                <p className="text-black text-xl  font-semi-bold border-b mt-8 hover:underline">Description</p>
                <p className="text-black text-md mt-2 ">{product.description} Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos commodi voluptatem dolor, veritatis, quos omnis iste eligendi id delectus animi nisi molestias, reiciendis rem nihil consequatur repellendus magni! Numquam, neque.</p>
            
            </div>
        </div>
    </div>
}
return(
    <div>
        {content}
</div>
)
}

export default ProductPage;
