import { CheckIcon, MenuIcon, XIcon } from "@heroicons/react/solid"
import Collapse from '../components/Collapse'
import Image from 'next/image'
import Link from 'next/link'



function Recap() {

    return (
        <div className="lg:w-4/5 lg:mr-auto lg:ml-auto">
            <header className="flex justify-between">
                <div className="flex items-center">
                    <MenuIcon className="h-14" />
                    <Link passHref={true} href="/products">
                        <Image src="/Assets/VERT_HD.png" alt="Logo" width={150} height={50} className="mr-auto ml-auto h-5  lg:h-10" />
                    </Link>
                    <p className="text-lg font-bold">Pro</p>
                </div>
            </header>
            <h1 className="text-3xl mt-8 text-center">Validation</h1>
            <Collapse/>

        </div>
    )
}

export default Recap
