import '../styles/globals.css'
import '../styles/carousel.css'
import '../styles/collapse.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
